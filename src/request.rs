//! Requests: in their canonical form (after processing of HTTP requests)

use serde::{Serialize, Deserialize};
use uuid::Uuid;

/// Encapsulates different types of requests for the chat service
#[derive(Debug)]
pub enum Request {
    Create(Create),
    Message(usize, Message),
    ListChats(ListChats),
    GetMessages(GetMessages),
}

/// Request to initiate a chat session between two participants/users.
#[derive(Serialize, Deserialize, Debug, Eq, PartialEq)]
#[serde(rename_all="camelCase")]
pub struct Create {
    pub id: usize,
    pub participant_ids: (usize, usize),
}

/// Actual message content requested to be sent from one
/// participant/user to another.
#[derive(Serialize, Deserialize, Debug, Eq, PartialEq)]
#[serde(rename_all="camelCase")]
pub struct Message {
    pub id: Uuid,
    pub timestamp: usize,
    pub message: String,
    pub source_user_id: usize,
    pub destination_user_id: usize,
}

/// Request to list chats for the specified User
#[derive(Debug)]
pub struct ListChats {
    pub user_id: usize,
}

/// Request to list a chat's messages
#[derive(Debug)]
pub struct GetMessages {
    pub chat_id: usize,
}
