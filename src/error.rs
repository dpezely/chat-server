use std::convert::From;

pub type Result<T> = std::result::Result<T, ErrorKind>;

#[derive(Debug)]
#[must_use]
pub enum ErrorKind {
    AddrParseError,
    BadMessage,                 // improperly formed
    BadRequest(&'static str),   // HTTP 400 Bad Request
    BufferTooSmall,
    ClientClosedConnection,
    EmptyMessage,
    NoFilePath,
    NotFound(&'static str),     // HTTP 404 Not Found
    ParseIntError,
    Unauthorized(&'static str), // HTTP 401 Unauthorized
    UnknownIoError,
    Utf8ParseError,
}

impl From<serde::de::value::Error> for ErrorKind {
    fn from(err: serde::de::value::Error) -> ErrorKind {
        println!("Serde error: {:?}", &err);
        ErrorKind::BadMessage
    }
}

impl From<serde_json::Error> for ErrorKind {
    fn from(err: serde_json::Error) -> ErrorKind {
        use serde_json::error::Category;
        match err.classify() {
            Category::Io => {
                println!("Serde JSON IO-error: {:?}", &err);
                ErrorKind::BadMessage
            }
            Category::Syntax | Category::Data | Category::Eof => {
                println!("Serde JSON error: {:?} {:?}", err.classify(), &err);
                ErrorKind::BadMessage
            }
        }
    }
}

impl From<std::io::Error> for ErrorKind {
    fn from(err: std::io::Error) -> ErrorKind {
        match err.kind() {
            std::io::ErrorKind::NotFound => {
                println!("File or directory path not found: {:?}", err);
                ErrorKind::NoFilePath
            }
            _ => {
                println!("IO Error: {:?}", err);
                ErrorKind::UnknownIoError
            }
        }
    }
}

impl From<std::io::ErrorKind> for ErrorKind {
    fn from(err: std::io::ErrorKind) -> ErrorKind {
        match err {
            std::io::ErrorKind::NotFound => {
                println!("File or directory path not found: {:?}", err);
                ErrorKind::NoFilePath
            }
            _ => {
                println!("IO Error: {:?}", err);
                ErrorKind::UnknownIoError
            }
        }
    }
}

impl From<std::net::AddrParseError> for ErrorKind {
    fn from(err: std::net::AddrParseError) -> ErrorKind {
        println!("Unable to use specified IP address: {:?}", &err);
        ErrorKind::AddrParseError
    }
}

impl From<std::num::ParseIntError> for ErrorKind {
    fn from(err: std::num::ParseIntError) -> ErrorKind {
        println!("Unable to parse integer: {:?}", &err);
        ErrorKind::ParseIntError
    }
}

impl From<std::str::Utf8Error> for ErrorKind {
    fn from(err: std::str::Utf8Error) -> ErrorKind {
        println!("UTF8 conversion error: {:?}", &err);
        ErrorKind::Utf8ParseError
    }
}
