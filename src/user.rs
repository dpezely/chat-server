//! Track everything about a user, excluding current State.

use serde::Deserialize;
use std::collections::{HashMap, HashSet};

use crate::error::Result;

/// Set of all users known by the service
pub type Users = HashMap<usize, Profile>;

/// Set of all users associated with a particular user
pub type Contacts = HashMap<usize, bool>;
    
/// Set of all chats for a single user: other User Id to Chat Id
pub type ChatSessions = HashSet<usize>;

/// Data about a user
pub struct Profile {
    pub id: usize,
    pub contacts: HashMap<usize, bool>, // Set of other user IDs
    pub chats: ChatSessions,            // NOT persisted to durable storage
}

/// This is the struct into which `serde` will parse JSON.
///
/// This differs from `Profile` because JSON object keys cannot be an
/// integer.  The structure here is required (rather than a `type`
/// alias) due to restrictions of `serde_json`...  Yes, there are
/// magic serde attributes for variants that probably address this
/// very issue, but this was easier for this implementation.
#[derive(Deserialize)]
struct ContactsJson {
    #[serde(flatten)]
    inner: HashMap<String, Vec<usize>>
}

/// Batch constructor of `Profile` instances for for populating `HashMap`
/// as `Users` type while loading file specified by `filepath`
pub fn load(filepath: &str) -> Result<Users> {
    let mut users: Users = HashMap::new();
    let json_bytes = std::fs::read(filepath)?;
    let utf8 = std::str::from_utf8(&json_bytes)?;
    let mut contacts: ContactsJson = serde_json::de::from_str(&utf8)?;
    for (user_id, contact_list) in contacts.inner.iter_mut() {
        let id = user_id.parse()?;
        let mut contacts: Contacts = HashMap::new();
        for user in contact_list {
            contacts.insert(*user, true);
        }
        users.insert(id, Profile{id, contacts, chats: HashSet::new()});
    }
    Ok(users)
}

impl Profile {
    pub fn is_connected(&self, other_id: usize) -> bool {
        self.contacts.contains_key(&other_id)
    }

    pub fn add_chat(&mut self, chat_id: usize) {
        self.chats.insert(chat_id);
    }
}
