//! Router using an event Loop

use lru::LruCache;   // hash-table with Least Recently Used heuristics
use mio::{Events, Poll, PollOpt, Ready, Token};
use mio::net::{TcpListener, TcpStream};
use std::io::{self, Read, Write};
use std::net::{SocketAddr, Shutdown};

use crate::chat::Chats;
use crate::http;
use crate::request::Request;
use crate::error::{Result, ErrorKind};
use crate::user::Users;

/// Configuration data for Router, used as immutable.
pub struct Router {
    listen_addr: SocketAddr,    // Bind IP address to listening socket
    max_clients: usize,         // Max open client sockets at any one time
    max_message_bytes: usize,
}

/// Client connection data: token id for socket mapped to socket stream
/// stored in a cache with Least Recently Used heuristics.
type Connections = LruCache<usize, Client>;

/// An inbound connection to the listening service:
/// Not to be confused with `Message` or `User`.
struct Client {
    stream: TcpStream,
    response: Vec<u8>,
}

impl Router {
    /// Constructor but returns Self within a Result.
    /// (therefore, don't call it "new()" per Rust style guide)
    pub fn create(listen_ip_addr: &str, listen_ip_port: u16,
                  max_clients: usize, max_message_bytes: usize) -> Result<Router> {
        let listen_addr = SocketAddr::new(listen_ip_addr.parse()?,
                                          listen_ip_port);
        Ok(Router{listen_addr, max_clients, max_message_bytes})
    }

    /// Message-router handles all network traffic.
    ///
    /// Listens for new inbound connections, reads messages, and attempts
    /// delivery of each message to the correct user.
    ///
    /// SIDE-EFFECTS: `users` gets its chronology of messages populated
    /// and modified.
    ///
    /// Uses "edge-triggered" events and each operation loops, ensures
    /// reading all delivered data: until receiving WouldBlock.
    /// Caveats explained: https://docs.rs/mio/0.6.19/mio/
    ///
    /// Uses `Token` to track which socket generated the notification.
    /// See https://docs.rs/mio/0.6.19/mio/struct.Token.html
    pub fn event_loop(&mut self, mut chats: Chats, mut users: Users) -> Result<()> {
        let listener = TcpListener::bind(&self.listen_addr)?;
        // Register the server's listening socket with `mio`:
        let poll = Poll::new()?;
        poll.register(&listener, Token(0), Ready::readable(), PollOpt::edge())?;

        let mut event_storage = Events::with_capacity(self.max_clients);
        let mut next_token_id: usize = 1;
        let mut connections: Connections = LruCache::new(self.max_clients);
        let mut buffer: Vec<u8> = Vec::with_capacity(self.max_message_bytes);
        for _ in 0..self.max_message_bytes { buffer.push(0); } // Set len = capacity
        loop {
            poll.poll(&mut event_storage, None)?; // wait for events
            for event in event_storage.iter() {
                match event.token() {
                    Token(0) => {
                        match self.accept(&poll, &listener,
                                          &mut connections, next_token_id) {
                            Ok(count) => {
                                next_token_id += count;
                                assert!(next_token_id < std::usize::MAX);
                            }
                            Err(e) => {
                                println!("Unexpected error={:?}", e);
                                continue
                            }
                        }
                    }
                    Token(token_id) => {
                        if event.readiness().is_readable() {
                            let mut closed = false;
                            if let Some(client) = connections.get_mut(&token_id) {
                                if let Ok(req) = self.receive(&mut client.stream,
                                                              &mut buffer) {
                                    let mut result = self.dispatch(req, &mut chats,
                                                                   &mut users);
                                    #[cfg(feature="verbose")]
                                    println!("{}", std::str::from_utf8(&result)?);
                                    client.response.append(&mut result);
                                } else {
                                    poll.deregister(&client.stream)?;
                                    client.stream.shutdown(Shutdown::Both)?;
                                    closed = true;
                                }
                            } else {
                                println!("ERROR: unknown token id={}", token_id);
                            }
                            if closed { // FIXME: salvage `token_id` value
                                connections.pop(&token_id);
                            }
                        }
                        if event.readiness().is_writable() {
                            if let Some(client) = connections.get_mut(&token_id) {
                                if client.response.len() > 0 {
                                    let _ = self.send(&mut client.stream,
                                                      &client.response);
                                    client.response.clear();
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /// Socket-level accept of new connection, read message, lookup User data.
    ///
    /// Inbound connected socket might be a proxy or load-balancer, so
    /// we cannot assume or conclude anything about which user-id or
    /// ids might come through this connection initially or in future.
    ///
    /// SIDE-EFFECTS: updates `connection` hash-table (LRU), and
    /// updates `poll` via its register() method.  Network sockets
    /// that fall off the back of the LRU cache will be closed when
    /// out of scope.
    fn accept(&self, poll: &Poll, listener: &TcpListener,
              connections: &mut Connections, token_id: usize) -> Result<usize> {
        // From mio: ALWAYS operate within a loop, and read until WouldBlock
        let mut count = 0;
        loop {
            match listener.accept() {
                Ok((stream, _addr)) => {
                    #[cfg(feature="verbose")]
                    println!("Listener accepting: addr={:?}", _addr);
                    // High probability that read() would block here,
                    // so register without tracking any User data.
                    // Maintainers of mio have commented that .register()
                    // causes epoll() on Linux to trigger an empty event.
                    let id = token_id + count;
                    count += 1;
                    poll.register(&stream, Token(id), 
                                  Ready::readable() | Ready::writable(),
                                  PollOpt::edge())?; 
                    connections.put(id, Client{stream: stream,
                                               response: Vec::new()});
                }
                Err(ref e) if e.kind() == io::ErrorKind::WouldBlock =>
                    break, // stop accepting for this batch
                Err(e) =>
                    panic!("Unexpected error={:?}", e)
            }
        }
        Ok(count)
    }

    /// Keep writing to `stream` until WouldBlock.
    fn send(&self, mut stream: &TcpStream, response: &[u8]) -> Result<()> {
        let mut offset = 0;
        loop {
            match stream.write(&response[offset..]) {
                Ok(0) => {
                    if offset > 0 {
                        break
                    } else {
                        return Err(ErrorKind::ClientClosedConnection)
                    }
                }
                Ok(n_bytes) => {
                    offset += n_bytes;
                    if offset == response.len() {
                        break
                    }
                }
                Err(ref e)
                    if e.kind() == io::ErrorKind::WouldBlock
                    || e.kind() == io::ErrorKind::Interrupted => {
                        break
                }
                Err(e) =>
                    panic!("Unexpected error={:?}", e)
            }
        }
        Ok(())
    }

    /// Keep reading from `stream` until WouldBlock.
    ///
    /// Use `serde` to parse JSON message and rely upon From methods to
    /// convert any error Result to our own ErrorKind.
    ///
    /// SIDE-EFFECTS: overwrites and then zeros `buffer`.
    fn receive(&self, mut stream: &TcpStream, buffer: &mut [u8]) -> Result<Request> {
        let mut offset = 0;
        loop {  
            match stream.read(&mut buffer[offset..]) {
                Ok(0) => {
                    if offset > 0 {
                        break
                    } else {
                        return Err(ErrorKind::ClientClosedConnection)
                    }
                }
                Ok(n_bytes) => {
                    offset += n_bytes;
                    if offset == self.max_message_bytes {
                        println!("ERROR: increase buffer capacity!");
                        return Err(ErrorKind::BufferTooSmall)
                    }
                }
                Err(ref e)
                    if e.kind() == io::ErrorKind::WouldBlock
                    || e.kind() == io::ErrorKind::Interrupted => {
                        break
                }
                Err(e) =>
                    panic!("Unexpected error={:?}", e)
            }
        }
        if offset > 0 {
            let response = http::parse(buffer, offset)?;
            for i in 0..offset {
                buffer[i] = 0;  // Ensure no payload leakage
            }
            Ok(response)
        } else {
            Err(ErrorKind::EmptyMessage)
        }
    }

    /// Dispatch various methods within `chat` module to do actual work.
    /// Then, calls back into `http` module to format response messages.
    ///
    /// Consumes request, `req`, to account for storage of message.
    /// SIDE-EFFECTS: `chats` gets its chronology of messages
    /// populated and modified, and `users` gets contacts and chats
    /// populated and modified.
    fn dispatch(&self, req: Request,
                chats: &mut Chats, users: &mut Users) -> Vec<u8> {
        #[cfg(feature="verbose")]
        println!("dispatch: request={:?}", &req);
        match req {
            Request::Create(request) => {
                http::response(chats.create_chat(users, request))
            }
            Request::Message(chat_id, request) => {
                http::response(chats.send_message(users, chat_id, request))
            }
            Request::ListChats(request) => {
                http::response(chats.list_chats(users, request))
            }
            Request::GetMessages(request) => {
                http::response(chats.get_messages(request))
            }
        }
    }
}
