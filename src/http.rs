//! Handle the HTTP protocol
//!
//! Be mindful of character encoding.  While HTTP headers are
//! conventionally ASCII or ISO-8859-1, newer RFCs account for other
//! character encoding schemes.  A conventional approach used by many
//! web browsers is to parse as ISO-8859-1 until encountering
//! Content-Type header, and then re-parse headers using that encoding.
//!
//! However, such rigour is beyond scope of this implementation.
//! This assumes UTF-8, of which ISO-8859-1 and ASCII conform as subsets.

use crate::error::{ErrorKind, Result};
use crate::request::{Request, Create, Message, ListChats, GetMessages};

pub fn parse(buffer: &[u8], byte_count: usize) -> Result<Request> {
    let utf8 = std::str::from_utf8(&buffer[0 .. byte_count])?;
    #[cfg(verbose)]
    println!("{}\n", utf8);
    // FIXME: some HTTP clients designate end of headers with "\n\n"
    let headers_body: Vec<&str> = utf8.splitn(2, "\r\n\r\n").collect();
    if headers_body.len() == 2 {
        // FIXME: process headers: body.len() matches Content-Length, gzip, etc.
        let headers: Vec<&str> = headers_body[0].split('\n').collect();
        let command: Vec<&str> = headers[0].split(' ').collect();
        let body = headers_body[1];
        let response =
            match command[0].to_uppercase().as_str() {
                "POST" => handle_post(command[1], body)?,
                "GET" => handle_get(command[1])?,
                _ => {
                    println!("Warning: bad HTTP request method={}", &command[0]);
                    let why = "Expected GET or POST";
                    return Err(ErrorKind::BadRequest(why))
                }
            };
        Ok(response)
    } else {
        Err(ErrorKind::BadMessage)
    }
}

/// Process `Result` from `chat` module, and generate HTTP response.
/// Any conversion such as to JSON must occur from the calling scope.
/// Calling scope may use results as `&[u8]` for `TcpStream::write()`.
pub fn response(result: Result<String>) -> Vec<u8> {
    match result {
        Ok(ref response) if response.len()==0 =>
            "HTTP/1.0 200 OK\r\n\r\n".to_string(), // Arguably could be 204
        Ok(response) =>
            ["HTTP/1.0 200 OK",
             "Content-Type: application/json; charset=utf-8",
             &format!("Content-Length: {}", response.len()),
             "",
             &response].join("\r\n"),
        Err(ErrorKind::BadMessage) =>
            response_message(400, "Message in request was malformed"),
        Err(ErrorKind::BadRequest(message)) =>
            response_message(400, message),
        Err(ErrorKind::NotFound(message)) =>
            response_message(404, message),
        Err(ErrorKind::Unauthorized(message)) =>
            response_message(401, message),
        Err(_) =>
            "HTTP/1.0 500 Internal Server Error\r\n\r\n".to_string()
    }.into_bytes()
}

fn response_message(response_code: usize, message: &str) -> String {
    let response =
        match response_code {
            400 => "400 Bad Request",
            401 => "401 Unauthorized",
            404 => "404 Not Found",
            _ => "501 Not Implemented", // i.e., "FIX ME"
        };
    [&format!("HTTP/1.0 {}", response),
     "Content-Type: text/plain; charset=utf-8",
     &format!("Content-Length: {}", message.len()),
     "",
     message].join("\r\n")
}

/// Accommodate these API endpoints:
/// - POST /chats - create a chat between two users
/// - POST /chats/{chatId}/messages - adds a message to a chat
fn handle_post(uri: &str, body: &str) -> Result<Request> {
    let parsed: Vec<&str> = uri.split('/').collect();
    if parsed[0] == "" && parsed[1] == "chats" {
        if parsed.len() == 2 {
            let create: Create = serde_json::de::from_str(body)?;
            Ok(Request::Create(create))
        } else if parsed.len() == 4 && parsed[3] == "messages" {
            let chat_id: usize = parsed[2].parse()?;
            let message: Message = serde_json::de::from_str(body)?;
            Ok(Request::Message(chat_id, message))
        } else {
            Err(ErrorKind::BadMessage)
        }
    } else {
        println!("Warning: unrecognized POST request uri={}", uri);
        Err(ErrorKind::BadMessage)
    }
}

/// Accommodate these API endpoints:
/// - GET /chats?userId={userId} - lists a user’s current chats
/// - GET /chats/{chatId}/messages - list a chat’s messages
fn handle_get(uri: &str) -> Result<Request> {
    let parsed: Vec<&str> = uri.split('/').collect();
    if parsed[0] == "" && &parsed[1][0..5] == "chats" {
        if parsed.len() == 2 && &parsed[1][5..13] == "?userid=" {
            let user_id = (&parsed[1][13..]).parse()?;
            Ok(Request::ListChats(ListChats{user_id}))
        } else if parsed.len() == 4 && parsed[3] == "messages" {
            let chat_id = (&parsed[2]).parse()?;
            Ok(Request::GetMessages(GetMessages{chat_id}))
        } else {
            println!("Warning: unrecognized GET /chat parsed={:?}", parsed);
            Err(ErrorKind::BadMessage)
        }
    } else {
        println!("Warning: unrecognized GET request uri={}", uri);
        Err(ErrorKind::BadMessage)
    }
}
