//! chat server - minimal due to constraints of a challenge

extern crate dotenv;
#[macro_use]
extern crate dotenv_codegen;
extern crate lru;
extern crate mio;
extern crate serde;
extern crate serde_json;
extern crate uuid;

mod chat;
mod error;
mod http;
mod request;
mod router;
mod user;

use dotenv::dotenv;

use crate::error::Result;
use crate::router::Router;

fn main() -> Result<()> {
    dotenv().ok();
    // File presumed to exist and be valid JSON.
    // Each top-level key is a string, value is small array with integers.
    const CONTACTS_FILEPATH: &'static str = dotenv!["CONTACTS_FILEPATH"];
    const LISTEN_IP_ADDR: &'static str = dotenv!["LISTEN_IP_ADDR"];
    const LISTEN_IP_PORT: &'static str = dotenv!["LISTEN_IP_PORT"];
    const MAX_CLIENTS: &'static str = dotenv!["MAX_CLIENTS"];
    const MAX_MESSAGE_BYTES: &'static str = dotenv!["MAX_MESSAGE_BYTES"];

    let users = user::load(CONTACTS_FILEPATH)?;
    println!("Loaded {} unique user contact lists", users.len());

    let chats = chat::Chats::new();
    
    println!("Listening: {}:{} ...", &LISTEN_IP_ADDR, &LISTEN_IP_PORT);
    Router::create(LISTEN_IP_ADDR, LISTEN_IP_PORT.parse()?,
                   MAX_CLIENTS.parse()?, MAX_MESSAGE_BYTES.parse()?)?
        .event_loop(chats, users)?;
    Ok(())
}
