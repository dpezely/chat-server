//! Chat Sessions

use std::collections::{BTreeMap, HashMap};

use crate::error::{ErrorKind, Result};
use crate::request::{Create, Message, ListChats, GetMessages};
use crate::user::Users;

/// Chat sessions organized by chat-id
pub struct Chats {
    sessions: HashMap<usize, ChatChronology>,
}

/// Chronology of chat messages within a single chat session,
/// for `usize` value of timestamps
pub type ChatChronology = BTreeMap<usize, Message>;

impl Chats {
    /// Constructor
    pub fn new() -> Self {
        Chats{sessions: HashMap::new()}
    }

    /// Create a new chat between a pair of users.
    /// Consumes `req`.
    // A separate immutable borrow is necessary for validity check *before*
    // making any modifications so that nothing needs to be undone.  These
    // initial checks couldn't be mutable because only one is allowed at a time.
    // Added block for `valid` isolates immutable borrow from mutable, later.
    // Compiler usually optimizes .unwrap() after .is_some() but not here
    // due to intermediate variable, `valid`.  Small cost for readability.
    pub fn create_chat(&mut self, users: &mut Users, req: Create) -> Result<String> {
        let (first_id, second_id) = req.participant_ids;
        let valid = {
            let first = users.get(&first_id);
            let second = users.get(&second_id);
            first.is_some() && second.is_some()
                && first.unwrap().is_connected(second_id)
                && second.unwrap().is_connected(first_id)
        };
        if valid {
            self.sessions.insert(req.id, BTreeMap::new());
            let user = users.get_mut(&first_id).unwrap();
            user.add_chat(req.id);
            let user = users.get_mut(&second_id).unwrap();
            user.add_chat(req.id);
            Ok("".to_string())
        } else {
            let why = "IDs must be valid and participants mutually connected";
            Err(ErrorKind::Unauthorized(why))
        }
    }

    /// Send a message to specified participants.
    /// Consumes `req`.
    pub fn send_message(&mut self, users: &mut Users,
                        chat_id: usize, req: Message) -> Result<String> {
        if let Some(chat) = self.sessions.get_mut(&chat_id) {
            let first = users.get(&req.source_user_id);
            let second = users.get(&req.destination_user_id);
            if first.is_some() && second.is_some() {
                chat.insert(req.timestamp, req);
                Ok("".to_string())
            } else {
                let why = "Participant IDs must be valid";
                Err(ErrorKind::NotFound(why))
            }
        } else {
            let why = "Chat ID must be valid";
            Err(ErrorKind::NotFound(why))
        }
    }

    pub fn list_chats(&self, users: &Users, req: ListChats) -> Result<String> {
        if let Some(user) = users.get(&req.user_id) {
            let chat_id_list: Vec<&usize> = user.chats.iter().collect();
            Ok(serde_json::to_string(&chat_id_list)?)
        } else {
            // Beware that this type of error message contributes to
            // side-channel attacks by helping a potential attacker
            // discover valid/invalid IDs... But acceptable for a toy.
            Err(ErrorKind::NotFound("Invalid User ID"))
        }
    }

    pub fn get_messages(&self, req: GetMessages) -> Result<String> {
        if let Some(chronology) = self.sessions.get(&req.chat_id) {
            let mut messages: Vec<&Message> = Vec::with_capacity(chronology.len());
            for msg in chronology.values() {
                messages.push(&msg);
            }
            Ok(serde_json::to_string(&messages)?)
        } else {
            Err(ErrorKind::NotFound("Invalid Chat ID"))
        }
    }
}
