Chat Server
===========

## Requirements

Basic Requirements:

- Spend 4-6 hours
- May use `mio` but otherwise only core Rust libraries for networking
- May use external libraries for JSON, etc.

API Endpoints:

- `POST /chats` -- create a chat between two users
- `POST /chats/{chatId}/messages` -- adds a message to a chat
- `GET /chats?userId={userId}` -- lists a user’s current chats
- `GET /chats/{chatId}/messages` -- list a chat’s messages

## Usage

**1.** Edit [.env](./.env) file for listening IP address, port number, and
    location of `contacts.json` (not supplied).  Note that the included
    versions specifies Port 80, per requirements of the challenge.

**2.** Build a release:

    cargo build --release
    
**3.** Run: use `sudo` if using a privileged IP port (below 1024)

    sudo ./target/release/chat-server
    
For examples of more production-oriented things like chained Dockerfiles
omitted here, please see:
[anagram-phrases-httpd](https://gitlab.com/dpezely/anagram-phrases-httpd)
on GitLab by same author.

**4.** For debugging, build (again) with `verbose` option:

    cargo clean --release -p $(cargo pkgid)
	cargo build --release --features=verbose

This prints the full HTTP request, each request after conversion from JSON,
and full HTTP response for every request.

## Design

This is fundamentally a router with ephemeral storage:  
Deliver messages while respecting filter constraints.

This is *single-threaded* using an I/O event loop, so there's much room for performance enhancements using co-routines (such as async/await), threads, etc.

In the Rust programming language, the I/O event loop used here is `mio`
("metal I/O" or originally, "minimal I/O").  It's essentially a wrapper around
`epoll()` on Linux, `kqueue()/kpoll()` on BSD Unix and macOS, etc.

It might be appealing at first to set each socket's Token ID for event loop
polling as a user-id.  However, there are two problems there: 1) we don't
yet have this information in time for registering its token ID; 2) we can't
be guaranteed of one connection per client due to network load balancers,
NAT, proxies, etc.

Instead, the loop begins with an initial seed of Token ID=1 and increments.

An unresolved issue for scope of this 4-6 hour project is handling closed
sockets, such as those that go out of scope due to expiring from the Least
Recently Used cache.  An LRU cache is used to accommodate some expiration.
(Giving only a little thought here, the LRU cache could be manually pruned
before putting a new element into it, and the popped Token ID could be
salvaged at that point.  The `index-pool` crate might work here.  Again,
that's not implemented.)

## Implementation

- Authenticates implicitly by confirming an entry exists within `contacts.json`
- Server might be running behind a network load-balancer, so inbound peer IP
  address gets ignored
  + Allows multiple requests for different user IDs on same socket
- Assuming `contacts.json` exists and is valid JSON
  + Confirmed with `jq`
  - Converted at run-time using `serde_json` 
- Assuming content of `contacts.json` remains small enough to be cached in RAM
- Store messages within BTreeMap to maintain chronology
- Uses `.env` file for parameters
  + Values assumed to be valid
- Without command-line parameters
- Without persistent storage
- Without validating UUIDs
  + They appear to conform to UUID v4
- IP addresses only tested using IPv4 for simplicity
- Without graceful shutdown-- must be killed to stop operations
- Uses fixed maximum message length (counting bytes)
  + A "buckets & brigade" chain would be overkill for this exercise
- Uses u64 for timestamps
  + Another option is Chronos library; e.g., if needing time zone
- Uses simple `ErrorKind` and `From` methods for error-handling:
  + Would prefer coalescing errors into a mini trace-back to maintain
    context since multi-threaded asynchronous servers are challenging enough
    to debug
  + The memory allocation argument *against* coalescing is important but
    less of a concern in recent years for server instances than embedded
    systems
- Logging to STDOUT
  + via `println`
  + output is essentially a transcript of HTTP request and response
  + with intermediate structs (converted via `serde_json`) sprinkled in between
- Ignoring gzip encoding for now
- Ignoring capacity regarding maximum bytes or characters allowed per message.
- Ignoring capacity regarding number of messages allowed in a single chat.
- Omitting `Date`, `Last-Modified` and other conventional fields from HTTP response.
  + Keeping only `Content-Type` and `Content-Length` fields.

## Notes About OS-Level Event Loops

Since `mio` is a thin wrapper around the OS event loop, it's worth noting
the underlying behaviour.  This applies to `epoll` on Linux, `kqueue` on BSD
Unix and macOS, and `select` on other branches of Unix.

### Handle False Events

It's important to know that there may be false events.

Calling `mio::poll::register()` will trigger such a false event.  Therefore,
the next event loop iteration may indicate that a stream is ready for read
but might not have anything to read, thus triggering
`io::ErrorKind::WouldBlock`.

### Sockets Being Writable Before Readable

A new TCP stream-- such as immediately after accepting from a listening
socket-- typically becomes writable before there's anything to read.

This makes sense when considering the 3-way handshake of a TCP/IP
connection.  IP packets might still be traversing the physical medium of
network cables at that particular nanosecond.

### Avoid Cloning A TCP Stream

Duplication of socket streams (e.g., using `.clone()` in Rust) can trigger
anomalous behaviour with `epoll` on Linux.

This gets called out by a contributor to `mio` itself in comments on past
issues but not obvious in the primary
documentation.[{1}](https://github.com/tokio-rs/mio/issues/966)
[{2}](https://github.com/pusateri/rslogd/issues/4)

This requires special attention for Rust programmers.

While writing early iterations of code and wrestling with the borrow checker
(`borrowck`) within the compiler, it not uncommon to simply clone a value,
add a "FIXME" comment and move on.

This, however, should be avoided.

Questions arise as to whether the clone operation is a simple Rust memory
management trick versus going all of the way into the network library stack
and performing a
[`dup(2)`](https://www.freebsd.org/cgi/man.cgi?query=dup&sektion=2) system
call that duplicates the file descriptor.  (There are a sufficient number of
dependencies for `mio` itself that this is left as an exercise for the
reader.  Also consider those Rust dependencies for different operating
systems than your own.  Have fun!)

### Array vs Vector For Buffer

Somewhat unique to Rust's [`read()`](https://doc.rust-lang.org/std/io/trait.Read.html#tymethod.read)
method of the Trait, `std::io::Read`:

Note that its `buf` parameter uses Array semantics-- not Vector-- even
though you may supply a vector when calling it.  (There's a different method,
[`read_to_end()`](https://doc.rust-lang.org/std/io/trait.Read.html#method.read_to_end)
that takes a mutable `Vec` but incompatible with non-blocking I/O.)

An Array has `.len()` for length which is comparable to a Vector's
`.capacity()`.  This means that for a vector within an array context, the
`.len()` method gets *different* semantics within the scope of `read()`.

Therefore, if allocating a Vector at run-time (e.g., when having a run-time
configurable buffer size rather than one that must be known at
compile-time), it must be pre-populated such that the vector's length
matches its capacity.

Conventionally, fill it with zeros.

If experiencing `WouldBlock` on *all* reads, re-examine Rust's documentation
for `std::io::read()` carefully, where it notes, "2. The buffer specified
was 0 bytes in *length*."  (Emphasis added.)  When supplying a Vector as
your buffer, it can be easy to conflate semantics of *length* with
*capacity*, which leads to this error.  Again, pre-filling the entire buffer
with zeros solves the issue.  Again, the main clue is that `read()`
specifies `&[u8]` rather than `Vec<u8>`.

### Read vs Write Events

A single event might indicate that a stream is ready for both writing and
reading.  Inside an application's event loop, it's important to test for
both with *two* `if` conditions (rather than `if-else`).

The decision about whether to handle reads before writes seems arbitrary,
but under Load & Capacity testing in much earlier versions of BSD and Linux
kernels, reading first was preferred.

Under extremely high network loads, reading too infrequently leads to errors
due to OS buffers getting filled, and the OS discarding packets.  (Such
errors can be tracked via `netstat` command on BSD Unix and Debian-based
Linux including Ubuntu.[{3}](https://www.freebsd.org/cgi/man.cgi?query=netstat)
[{4}](https://linux.die.net/man/8/netstat))

### Load & Capacity

Load & Capacity test and measurement was beyond the scope of implementation
for the coding challenge but should be performed as a matter of course for
applications involving such event loops.

The rationale here is that you truly don't know until it's been measured.

There are simply too many variables in that mental model, and many change
over time with the release of new OS kernels.

### Conclusion

There are lots of caveats in Unix systems programming, and working with
event loops for TCP/IP network socket stream seem particularly finicky.

Having benefited from a short write-up like this while I was learning the C
equivalent many years ago, hopefully someone else might appreciate insights
from this one.
